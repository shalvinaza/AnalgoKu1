/*
Nama        : Shalvina Zahwa Aulia
NPM         : 140810180052
Kelas       : B
Deskripsi   : Stable Matching Problem Program
*/
#include <iostream>
#include <string.h>
#include <stdio.h>
using namespace std;

#define N 5

 bool Man(int prior[2*N][N], int w, int m, int m1){
    for(int i=0; i<N; i++){
        if(prior[w][i] == m1)
            return true;
        if(prior[w][i] == m)
            return false;
    }
 }
 void engaged(int prior[2*N][N]){
    int womanFiance[N];

    bool freeMan[N];

    memset(womanFiance,-1,sizeof(womanFiance));
    memset(freeMan,false,sizeof(freeMan));
    int jmlFreeMan = N;

    while(jmlFreeMan > 0){
        int m;
        for(m=0 ; m<N; m++){
            if(freeMan[m] ==  false)
                break;
        }
        for(int i=0; i<N && freeMan[m] == false; i++){
            int w = prior[m][i];
            if(womanFiance[w-N] == -1){
                womanFiance[w-N] = m;
                freeMan[m]=true;
                jmlFreeMan--;
            }
            else{
                int m1 = womanFiance[w-N];
                if(Man(prior,w,m,m1) == false){
                    womanFiance[w-N] = m;
                    freeMan[m] = true;
                    freeMan[m1] = false;
                }
            }
        }
    }
        string man;
    string woman;
    for(int i=0; i<N; i++){
        if(i<N){
            if(womanFiance[i] == 0)
                man = "Victor";
            if(womanFiance[i] == 1)
                man = "Wyatt";
            if(womanFiance[i] == 2)
                man = "Xavier";
            if(womanFiance[i] == 3)
                man = "Yancey";
            if(womanFiance[i] == 4)
                man = "Zeus";
            if (i == 0)
                woman = "Amy";
            if (i == 1)
                woman = "Bertha";
            if (i == 2)
                woman = "Clare";
            if (i == 3)
                woman = "Diane";
            if (i == 4)
                woman = "Erika";
        }
        cout << "Engaged ( " << man << ", " << woman << " )" <<endl;
    }
    cout << endl;
 }
int main(){
    int prior[2*N][N]={{6,5,8,9,7},
                       {8,6,5,7,9},
                       {6,9,7,8,5},
                       {5,8,7,6,9},
                       {6,8,5,9,7},
                       {4,0,1,3,2},
                       {2,1,3,0,4},
                       {1,2,3,4,0},
                       {0,4,3,2,1},
                       {3,1,4,2,0}};
    engaged(prior);
    return 0;
}
